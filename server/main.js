import { Meteor } from 'meteor/meteor';
import { Players } from './../imports/api/players';

Meteor.startup(() => {

});

let home = {
  bedrooms: 2,
  bathrooms: 2
}

let yearBuilt = 1990;

let house = {
  yearBuilt: yearBuilt,
  ...home,
  bedrooms: 3,
}

console.log(house);
