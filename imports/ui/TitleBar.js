import React, { Component } from 'react';
import propTypes from 'prop-types';


export default class TitleBar extends Component {

    rendersubtitle = () => {
        if(this.props.subtitle) {
            return <h2>{this.props.subtitle}</h2>
        }
    }
    render() {
        return (
            <div className="header-bar">
                <div className="wrapper">
                    <h1 className="title">{this.props.title}</h1>
                    <h4 className="subtitle">{this.props.subtitle}</h4>
                </div>
            </div>
        );
    }
}

TitleBar.propTypes = {
    title: propTypes.string.isRequired,
    subtitle: propTypes.string.isRequired,
}