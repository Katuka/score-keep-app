import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Players } from './../api/players';




export default class AddPlayer extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        let playerName = e.target.playerName.value;
        if(playerName) {
          Players.insert({
            name: playerName,
            score: 0
          })
        e.target.playerName.value = '';
        }
    }

    render(){
        return(
            <div className="item">
                <form  className="form" onSubmit={this.handleSubmit}>
                    <input  className="form__input" type="text" name="playerName" placeholder="Add player name..." />
                    <button className="button">Add player</button>
                </form>
            </div>
        );
    }
}