import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Players, calculatePlayerPositions } from './../imports/api/players';
import { Tracker } from 'meteor/tracker';
import App from './../imports/ui/App';


setTimeout(() => {
  Meteor.startup(() => {
    Tracker.autorun(() => {
      let players = Players.find({}, {
        sort: {score: -1}
      }).fetch();

      let positionedPlayers = calculatePlayerPositions(players);
      let title = 'Score Keep';
      let subtitle = '~By Enoch Katebe Katuka';
      ReactDOM.render(<App title={title} subtitle={subtitle} players={positionedPlayers}/>, document.getElementById('container'));
    });
  });
}, 300)

